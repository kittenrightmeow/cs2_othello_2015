#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int scoreMove(Move *ourMove);
    int scoreMinimax(Move *ourMove);
    int scoreMinimax2(Move *opponentsMove, Board *tempBoard);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board *gameboard;
private:
	Side our_side;
	Side opp_side;
	Move *move;

};

#endif
