#include "player.h"

/* Meow. */
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) 
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    // Initialize a new Board.
    gameboard = new Board();
    move = NULL;

    // Define our team's side and the opponent's side.
    our_side = side;
    if (our_side == BLACK)
    {
        opp_side = WHITE;
    }
    else
    {
        opp_side = BLACK;
    }
}

/*
 * Destructor for the player.
 */
Player::~Player() 
{
    delete gameboard;
}



int Player::scoreMove(Move *ourMove)
{
    int score;
    // Create a copy of the board and make the potential move. 
    Board *temp_board = gameboard->copy();
    temp_board->doMove(ourMove, our_side);
    // Calculate the preliminary score based on the number of stones.
    if (our_side == BLACK)
    {
        score = temp_board->countBlack() - temp_board->countWhite();
    }
    else
    {
        score = temp_board->countWhite() - temp_board->countBlack();
    }

    // Update the score if a position is particularly desirable or undesirable.
    // If the move is to a corner position, improve score.
    if ((ourMove->x == 0 && ourMove->y == 0) || (ourMove->x == 0 && ourMove->y == 7) ||
     (ourMove->x == 7 && ourMove->y == 0) || (ourMove->x == 7 && ourMove->y == 7))
    {
        score += 5;
    }
    // If the move is to a position adjacent to a corner, decrease score.
    else if ((ourMove->x == 0 && ourMove->y == 1) || (ourMove->x == 1 && ourMove->y == 0) || 
        (ourMove->x == 1 && ourMove->y == 1) || (ourMove->x == 6 && ourMove->y == 0) || 
        (ourMove->x == 6 && ourMove->y == 1) || (ourMove->x == 7 && ourMove->y == 1) ||
        (ourMove->x == 0 && ourMove->y == 6) || (ourMove->x == 1 && ourMove->y == 6) ||
        (ourMove->x == 1 && ourMove->y == 7) || (ourMove->x == 6 && ourMove->y == 6) ||
        (ourMove->x == 6 && ourMove->y == 7) || (ourMove->x == 7 && ourMove->y == 6))
    {
        score -= 5;
    }
    delete temp_board;
    return score;
}

int Player::scoreMinimax(Move *ourMove)
{
    int minimax_score = 1000;

    // Create a copy of the gameboard and make the move.
    Board *temp_board = gameboard->copy();
    temp_board->doMove(ourMove, our_side);

    // If this move does not finish the game, implement a 2-ply minimax to 
    // decide the best move.
    if (gameboard->isDone() == false)
    {
        Move *opp_move = NULL;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                opp_move = new Move(x, y);
                // For all valid moves by the opponent, find the minimimum
                // score. 
                if (temp_board->checkMove(opp_move, opp_side) == true)
                {
                    int temp_score = scoreMinimax2(opp_move, temp_board);
                    // Update the score if a position is particularly desirable or undesirable.
                    // If the move is to a corner position, improve score.
                    if ((ourMove->x == 0 && ourMove->y == 0) || (ourMove->x == 0 && ourMove->y == 7) ||
                     (ourMove->x == 7 && ourMove->y == 0) || (ourMove->x == 7 && ourMove->y == 7))
                    {
                        temp_score += 5;
                    }
                    // If the move is to a position adjacent to a corner, decrease score.
                    else if ((ourMove->x == 0 && ourMove->y == 1) || (ourMove->x == 1 && ourMove->y == 0) || 
                        (ourMove->x == 1 && ourMove->y == 1) || (ourMove->x == 6 && ourMove->y == 0) || 
                        (ourMove->x == 6 && ourMove->y == 1) || (ourMove->x == 7 && ourMove->y == 1) ||
                        (ourMove->x == 0 && ourMove->y == 6) || (ourMove->x == 1 && ourMove->y == 6) ||
                        (ourMove->x == 1 && ourMove->y == 7) || (ourMove->x == 6 && ourMove->y == 6) ||
                        (ourMove->x == 6 && ourMove->y == 7) || (ourMove->x == 7 && ourMove->y == 6))
                    {
                        temp_score -= 5;
                    }
                    if (temp_score < minimax_score)
                    {
                        minimax_score = temp_score;
                    }
                }
            }
        }
    }
    // If the move finishes the game, the score is simply the difference in 
    // number of stones.
    else
    {
        if (our_side == BLACK)
        {
            minimax_score = temp_board->countBlack() - temp_board->countWhite();
        }
        else
        {
            minimax_score = temp_board->countWhite() - temp_board->countBlack();
        }
    }
    return minimax_score;
}

// This function calculates the score after an opponent has made their move.
// Takes the opponent's potential move and a temporary board that is one-ply 
// deep.
int Player::scoreMinimax2(Move *opponentsMove, Board *tempBoard)
{
    int score;
    // Create a copy of the board and make the potential move. 
    Board *temp_board = tempBoard->copy();
    temp_board->doMove(opponentsMove, opp_side);
    // Calculate the score based on the number of stones.
    if (our_side == BLACK)
    {
        score = temp_board->countBlack() - temp_board->countWhite();
    }
    else
    {
        score = temp_board->countWhite() - temp_board->countBlack();
    }

    // If the move is to a corner position, improve score.
    if ((opponentsMove->x == 0 && opponentsMove->y == 0) || (opponentsMove->x == 0 && opponentsMove->y == 7) ||
     (opponentsMove->x == 7 && opponentsMove->y == 0) || (opponentsMove->x == 7 && opponentsMove->y == 7))
    {
        score -= 5;
    }
    // If the move is to a position adjacent to a corner, decrease score.
    else if ((opponentsMove->x == 0 && opponentsMove->y == 1) || (opponentsMove->x == 1 && opponentsMove->y == 0) || 
        (opponentsMove->x == 1 && opponentsMove->y == 1) || (opponentsMove->x == 6 && opponentsMove->y == 0) || 
        (opponentsMove->x == 6 && opponentsMove->y == 1) || (opponentsMove->x == 7 && opponentsMove->y == 1) ||
        (opponentsMove->x == 0 && opponentsMove->y == 6) || (opponentsMove->x == 1 && opponentsMove->y == 6) ||
        (opponentsMove->x == 1 && opponentsMove->y == 7) || (opponentsMove->x == 6 && opponentsMove->y == 6) ||
        (opponentsMove->x == 6 && opponentsMove->y == 7) || (opponentsMove->x == 7 && opponentsMove->y == 6))
    {
        score += 5;
    }
    return score;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    
    // Process the opponent's move.

    gameboard->doMove(opponentsMove, opp_side);
    
    if (testingMinimax == true)
    {
        // Check that valid moves remain. 
        if (gameboard->isDone() == false)
        {
            int best_score = -1000;
            Move *best_move = NULL;
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    move = new Move(x,y);
                    // For all positions (x, y), if a move is valid, calculate
                    // its score. Find the move that gives the highest score.
                    if (gameboard->checkMove(move, our_side) == true)
                    {
                        int temp_score = this->scoreMove(move);
                        if (temp_score > best_score)
                        {
                            best_move = move;
                            best_score = temp_score;
                        }
                    }
                }
            }
            // Make the best move and return.
            gameboard->doMove(best_move, our_side);
            return best_move;
        }
        // If the game is done, return.
        return NULL;
    }

    else 
    {
        // Check that valid moves remain.
        if (gameboard->isDone() == false)
        {
            int best_score = -1000;
            Move *best_move = NULL;
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    move = new Move(x,y);
                    // For all positions (x, y), check that a move is valid. If 
                    // yes, calculate the minimax score. 
                    if (gameboard->checkMove(move, our_side) == true)
                    {
                        int temp_score = this->scoreMinimax(move); 
                        if (temp_score > best_score)
                        {
                            best_move = move;
                            best_score = temp_score;
                        }
                    }
                }
            }
            // Make the move with the highest minimax score and return.
            gameboard->doMove(best_move, our_side);
            return best_move;
        }
        // If the game is done, return.
        return NULL;
    }
}


